local fcgi = require( "fcgi" )

while fcgi.accept() do
	fcgi.print( "Content-Type: text/plain; charset=utf-8\r\n\r\n" )
	fcgi.print( "uri\t" .. fcgi.getenv( "DOCUMENT_URI" ) .. "\n" )
	fcgi.print( "agent\t" .. fcgi.getenv( "HTTP_USER_AGENT" ) .. "\n" )
	fcgi.print( "query\t" .. fcgi.getenv( "QUERY_STRING" ) .. "\n" )
	fcgi.print( "post\t" .. fcgi.post() .. "\n" )
	fcgi.print( "env\n" )
	fcgi.dumpenv()
end

-- From https://github.com/mikejsavage/lua-fcgi